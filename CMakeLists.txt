cmake_minimum_required(VERSION 3.5)

project(pisces-kwin-plugins)

set(PROJECT_NAME "pisces-kwin")

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

find_package(ECM REQUIRED NO_MODULE)
list(APPEND CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

include(GNUInstallDirs)
include(KDEInstallDirs)

find_package(KF5CoreAddons REQUIRED)
find_package(KF5Config REQUIRED)
find_package(KF5WindowSystem REQUIRED)
find_package(KDecoration2 REQUIRED)
find_package(Qt5 CONFIG REQUIRED COMPONENTS Gui Widgets Core X11Extras)

add_subdirectory(plugins)

install(FILES config/kglobalshortcutsrc DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}/xdg)
install(FILES config/kwinrc DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}/xdg)
install(FILES config/kwinrulesrc DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}/xdg)

install(DIRECTORY scripts/pisceslauncher DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/kwin/scripts)
install(DIRECTORY scripts/pisces_squash DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/kwin/effects)
install(DIRECTORY scripts/pisces_scale DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/kwin/effects)
install(DIRECTORY scripts/pisces_popups DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/kwin/effects)
install(DIRECTORY tabbox/pisces_thumbnail DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/kwin/tabbox)
